﻿import java.text.DecimalFormat;

public class TestAccount2 {

    private static DecimalFormat df1 = new DecimalFormat("#.##");

    public static void main(String args[]) {
        Account acc1 = new Account("Amy", 28.48);
        Account acc2 = new Account("Bob", 34.58);
        Account acc3 = new Account("Cat", 49.01);
        Account acc4 = new Account("Dave", 84.72);
        Account acc5 = new Account("Em", 82.95);
        Account[] arrayOfAccounts = {acc1, acc2, acc3, acc4, acc5};

        Account.setInterestRate(0.2);

        for (int i=0; i<=4; i++) {
            System.out.println("Account Name: " + arrayOfAccounts[i].getName());
            System.out.println("Account Balance: £" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Balance After Interest: £" + df1.format(arrayOfAccounts[i].getBalance()));
            System.out.println("");
        }

        acc1.withdraw(30.0);
        System.out.println("A's account after withdrawal: £" + df1.format(acc1.getBalance()));
        acc2.withdraw20();
        System.out.println("B's account after withdrawl: £" + df1.format(acc2.getBalance()));
    }
    
}
