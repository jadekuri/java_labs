public class Exercise3 {

    public static void main (String args[]) {
        String make = "Mini", model = "Cooper";
        double engineSize = 2;
        byte gear = 2;
        short speed = (byte) (gear*20);

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        System.out.println("The speed is " + speed + "mph");

        if (engineSize <= 1.3) {
            System.out.println("The car is weak");
        }
        else {
            System.out.println("The car is powerful");
        }
   
    }
    
}
