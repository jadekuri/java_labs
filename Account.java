public class Account {

    private double balance;
    private String name;
    private static double interestRate;

    public Account() {
        name = "Jade Gabriel";
        balance = 50;
    }

    public Account(String name, double balance) {
        this.balance = balance;
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double intRate) {
        interestRate = intRate;
    }

    public void addInterest() {
        balance = balance + (balance * interestRate);
    }

    public boolean withdraw(double amount) {
        if(amount < balance) {
            balance = balance - amount;
            return true;
        } else {
            return false;
        }
    }

    public boolean withdraw20() {
        if (balance >= 20) {
            balance = balance - 20;
            return true;
        } else {
            return false;
        }
    }
    
}
