public class Exercise4 {

    public static void main(String args[]){
        int year = 1900;
        int noOfLeapYears = 0;

        while((year <= 2000) && (noOfLeapYears < 5)) {
            if(year%4 == 0) {
                 System.out.println(year); 
                 noOfLeapYears++;          
            }
            year++;
        }

        
    }    
}
