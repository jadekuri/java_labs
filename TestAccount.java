﻿import java.text.DecimalFormat;

public class TestAccount {

    private static DecimalFormat df1 = new DecimalFormat("#.##");

    public static void main(String args[]) {
        Account myAccount = new Account();

        myAccount.setName("Jade Gabriel");
        myAccount.setBalance(1000.50);

        System.out.println("Account Name: " + myAccount.getName());
        System.out.println("Account Balance: £" + df1.format(myAccount.getBalance()));

        myAccount.addInterest();
        System.out.println("Balance After Interest: £" + df1.format(myAccount.getBalance()));

        System.out.println("");

        Account[] arrayOfAccounts = new Account[4];
        double[] amounts = {23, 5444, 2, 345, 34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i=0; i<=4; i++) {
            Account account = new Account(names[i], amounts[i]);
            arrayOfAccounts[i] = account;
            System.out.println("Account Number: " + i+1);
            System.out.println("Account Name: " + names[i]);
            System.out.println("Account Balance: £" + df1.format(amounts[i]));
            account.addInterest();
            System.out.println("Balance After Interest: £" + df1.format(account.getBalance()));
            System.out.println("");
        }

    }
    
}
